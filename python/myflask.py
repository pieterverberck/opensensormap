from flask import Flask, request

import classifier
import base64
import os
import sys
import json

app = Flask(__name__)

@app.route('/inference', methods=['POST'])
def hello_world():

    print("request.data", file=sys.stderr)
    print(request.data, file=sys.stderr)
    print(len(request.data), file=sys.stderr)
    parsedReq = json.loads(request.data)
    filename = parsedReq["filename"]
    decoded_string = base64.b64decode(parsedReq["data"])
    print("decoded_string", file=sys.stderr)
    print(len(decoded_string), file=sys.stderr)
    #print(decoded_string.decode("utf-8"), file=sys.stderr)
    with open('/' + filename, 'wb') as file:
        file.write(decoded_string)
    
    file_stats = os.stat('/' + filename)
    print(file_stats, file=sys.stderr)
    
    

    return classifier.classify_file('/' + filename)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6001)