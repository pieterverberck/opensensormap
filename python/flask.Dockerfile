# Use an official Python runtime as a parent image
FROM python:3.11

# Set the working directory in the container to /app
WORKDIR /

# Add the current directory contents into the container at /app
ADD . /

# Install any needed packages specified in requirements.txt
RUN pip install Flask jinja2

# Run app.py when the container launches
CMD ["python", "myflask.py"]