import tensorflow as tf
import tensorflow_hub as hub
import librosa
import pandas as pd
import numpy as np
from scipy.signal import butter, lfilter, freqz
import matplotlib.pyplot as plt
import subprocess
import os.path
import shutil

def butter_lowpass(cutoff, fs, order=5):
    return butter(order, cutoff, fs=fs, btype='low', analog=False)

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def classify_file(path):
  yamnet_model_handle = '/var/model/9616fd04ec2360621642ef9455b84f4b668e219e'
  yamnet_model = hub.load(yamnet_model_handle)

  new_path = os.path.splitext(path)[0]+'.mp3'
  subprocess.run(["ffmpeg", "-i", path, new_path])
  shutil.copyfile(new_path, "/var/data/" + os.path.basename(new_path))

  obs, sr = librosa.load(new_path)
  # obs = obs / (obs.max() - obs.min()) # normalize

  class_map_path = yamnet_model.class_map_path().numpy().decode('utf-8')
  class_names =list(pd.read_csv(class_map_path)['display_name'])

  order = 6
  fs = sr
  cutoff = 2e4
  #obs = butter_lowpass_filter(obs, cutoff, fs, order)

  scores, embeddings, spectrogram = yamnet_model(obs)
  class_scores = tf.reduce_mean(scores, axis=0)
  top_class = tf.math.argmax(class_scores)
  inferred_class = class_names[top_class]

  return(inferred_class)