import express from 'express';

import * as fs from 'fs';
import axios from 'axios';
import { performance } from 'perf_hooks';

function decodeBase64FileAndWrite(base64String: string, outputPath: string) {
    // create a buffer from the base64 encoded string
    const buffer = Buffer.from(base64String, 'base64');
    
    // write the buffer to a file
    fs.writeFileSync(outputPath, buffer);
}

interface Activity {
    timestamp: string,
    longitude: number,
    latitude: number,
    sensor_type: string,
    data: string,
}

interface Event {
    longitude: number,
    latitude: number,
    classification: string,
    filename: string,
}

interface Inference {
    data: string,
    filename: string
}

const app = express();
app.use(express.json()); // for parsing application/json

async function postData(url: string, data: string, headers: any): Promise<string> {
    try {
        const response = await axios.post(url, data, headers);
        console.log("##########################");
        console.log(response.data);
        console.log("##########################");
        return response.data
    } catch (error) {
        console.error(`Error fetching data from ${url}:`, error);
    }
    return ""
}

app.post('/sensor-activity', async (req, res) => {
    console.log('POST sensor-activity')
    console.log(Date.now().toString())
    //console.log(req);
    //console.log(req.body); // your data will be available through req.body
    let activity: Activity = req.body;
    //console.log(activity.data);
    let startTime = performance.now();
    let filename = Date.now().toString() + '_sound_for_inference.aac';
    let inference: Inference = { data: activity.data, filename: filename }
    let answer = await postData('http://host.docker.internal:6001/inference', JSON.stringify(inference), { headers: { 'Content-Type': 'application/json' } })
    let endTime = performance.now();
    let timeDiff = endTime - startTime; // in ms 
    console.log("Inferencing took ", timeDiff, " ms");
    let event: Event = { latitude: activity.latitude, longitude: activity.longitude, classification: answer, filename: filename }
    console.log(JSON.stringify(event));
    postData('http://host.docker.internal:8000/event', JSON.stringify(event), { headers: { 'Content-Type': 'application/json' } })
    //let fileName = '/Users/pieterverberck/dev/ITP/bot/postedrecording.mp3'
    //decodeBase64FileAndWrite(activity.data, fileName)

    res.json({ message: 'POST request received' });
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});