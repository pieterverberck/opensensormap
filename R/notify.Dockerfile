FROM rocker/r-ver:4.0.0

WORKDIR /src

RUN apt-get update && apt-get install -y --no-install-recommends \
  git-core \
  libssl-dev \
  libz-dev \
  libcurl4-gnutls-dev \
  libsodium-dev

RUN install2.r --error plumber
RUN R -e "install.packages('remotes')"
RUN R -e "remotes::install_cran('jsonlite')"

COPY ./event-api.R /var
COPY ./plumber.R /var

EXPOSE 8000
ENTRYPOINT ["R", "-f", "/var/plumber.R", "--slave"]
