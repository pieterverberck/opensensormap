FROM rocker/shiny:4.1.0

RUN apt-get update -qq && apt-get install -y \
  git-core \
  libssl-dev \
  libcurl4-gnutls-dev

RUN R -e "install.packages('remotes')"

RUN R -e "remotes::install_cran(c('leaflet', 'shinyjs'))"

RUN rm -rf /srv/shiny-server/*

COPY ./shiny-server.conf /etc/shiny-server/shiny-server.conf

WORKDIR /srv/shiny-server/

COPY ./shiny.R ./app.R

COPY ./alert.png ./alert.png

EXPOSE 3838
